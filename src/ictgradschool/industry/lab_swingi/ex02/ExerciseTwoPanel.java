package ictgradschool.industry.lab_swingi.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JButton addButton;
    private JButton subtractButton;
    private JTextField firstText;
    private JTextField secondText;
    private JTextField resultText;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.firstText = new JTextField(10);
        this.add(firstText);

        this.secondText = new JTextField(10);
        this.add(secondText);

        this.addButton = new JButton("Add");
        this.add(addButton);
        this.addButton.addActionListener(this);

        this.subtractButton = new JButton("Subtract");
        this.add(subtractButton);
        this.subtractButton.addActionListener(this);

        this.resultText = new JTextField(20);
        this.add(resultText);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        double firstNumber = Double.parseDouble(firstText.getText());
        double secondNumber = Double.parseDouble(secondText.getText());

        double addTotal = firstNumber + secondNumber;
        double addDecimal = roundTo2DecimalPlaces(addTotal);

        double subtractTotal = firstNumber - secondNumber;
        double subtractDecimal = roundTo2DecimalPlaces(subtractTotal);

        if (e.getSource() == addButton) {
            resultText.setText(String.valueOf(addDecimal));
        } else if (e.getSource() == subtractButton) {
            resultText.setText(String.valueOf(subtractDecimal));
        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }


}
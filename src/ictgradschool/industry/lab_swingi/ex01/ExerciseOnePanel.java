package ictgradschool.industry.lab_swingi.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField textBMI;
    private JTextField textHealthyWeight;
    private JTextField textWeight;
    private JTextField textHeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        this.calculateBMIButton = new JButton("Calculate BMI: ");

        this.calculateHealthyWeightButton = new JButton("Calculate Healthy Weight: ");

        this.textBMI = new JTextField(10);

        this.textHealthyWeight = new JTextField(10);

        this.textWeight = new JTextField(20);

        this.textHeight = new JTextField(20);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightMetresLabel = new JLabel("Height in Metres: ");

        JLabel weightKilogramsLabel = new JLabel("Weight in Kilograms: ");

        JLabel weightBMILabel = new JLabel("Your Body Mass Index (BMI) is: ");

        JLabel maxHealthyWeightLabel = new JLabel("Maximum healthy weight for your height: ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

//        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
//        this.setLayout(layout);

        this.add(heightMetresLabel);
        this.add(textHeight);
        this.add(weightKilogramsLabel);
        this.add(textWeight);
        this.add(calculateBMIButton);
        this.add(weightBMILabel);
        this.add(textBMI);
        this.add(calculateHealthyWeightButton);
        this.add(maxHealthyWeightLabel);
        this.add(textHealthyWeight);


        // TODO Add Action Listeners for the JButtons

        this.calculateBMIButton.addActionListener(this);

        this.calculateHealthyWeightButton.addActionListener(this);

    }

    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        double heightD = Double.parseDouble(textHeight.getText());
        double weightD = Double.parseDouble(textWeight.getText());
        double totalBMI = weightD / (heightD * heightD);
        double decimalBMI = roundTo2DecimalPlaces(totalBMI);

        double totalMaxWeight = 24.9 * heightD * heightD;
        double decimalMaxWeight = roundTo2DecimalPlaces(totalMaxWeight);

        if (event.getSource() == calculateBMIButton) {
            textBMI.setText(String.valueOf(decimalBMI));
        } else if (event.getSource() == calculateHealthyWeightButton) {
            textHealthyWeight.setText(String.valueOf(decimalMaxWeight));
        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
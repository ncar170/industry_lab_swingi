package ictgradschool.industry.lab_swingi.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    private  JButton moveButton;
    private Timer timer;

    private List<Balloon> balloonList;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloonList = new ArrayList<>();

        this.balloon = new Balloon(30, 60);

        for (int i = 0; i < 10; i++){
            int left = (int)(Math.random()*100);
            int top = (int)(Math.random()*100);
            addBalloon(new Balloon(left, top));
        }

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);

        this.addKeyListener(this);

        this.timer = new Timer(200, this);

    }

    public void addBalloon(Balloon balloon) {
        balloonList.add(balloon);
        repaint();
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == moveButton) {
            if (timer.isRunning()){
                timer.stop();
                balloon.move();
            } else {
                timer.start();
                balloon.move();
            }
        }

        balloon.move();

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < balloonList.size(); i++){
            Balloon b = (Balloon)balloonList.get(i);
            b.draw(g);
        }
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                timer.start();
                balloonList.get(0).setDirection(Direction.Right);
                balloonList.get(0).move();
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                timer.start();
                balloonList.get(0).setDirection(Direction.Left);
                balloonList.get(0).move();
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                timer.start();
                balloonList.get(0).setDirection(Direction.Up);
                balloonList.get(0).move();
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                timer.start();
                balloonList.get(0).setDirection(Direction.Down);
                balloonList.get(0).move();
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_S){
                timer.stop();
            }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}